package br.com.guiabolso.gbfrontapp.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.guiabolso.gbfrontapp.R
import br.com.guiabolso.gbfrontapp.model.CategoryImageData
import br.com.guiabolso.gbfrontapp.model.JokeResponse
import br.com.guiabolso.gbfrontapp.model.ViewTypesListener
import kotlinx.android.synthetic.main.jokes_category_list.view.*


class JokeCategoriesAdapter(private val categories: Array<String>,
                            private val context: Context,
                            private val listener: ViewTypesListener<String>) :
        RecyclerView.Adapter<JokeCategoriesAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val categoryView = LayoutInflater.from(context)
                .inflate(R.layout.jokes_category_list, parent, false)
        return MyViewHolder(categoryView, listener)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val category = categories[position]
        holder.bindView(category)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    class MyViewHolder(val categoriesView: View, val listener: ViewTypesListener<String>)
        : RecyclerView.ViewHolder(categoriesView) {
        fun bindView(category: String) {
            val description = categoriesView.category_title
            description.text = category
            val imageID = getImageID(category)
            categoriesView.imageView.setImageResource(imageID)
            categoriesView.setOnClickListener { listener.invoke(category) }
        }

        private fun getImageID(category: String): Int {
            return try {
                CategoryImageData().getDrawbleImage(category)
            } catch (e: NoSuchElementException) {
                Log.e("image not found", e.toString())
            }
        }
    }
}