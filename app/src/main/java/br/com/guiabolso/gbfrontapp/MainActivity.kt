package br.com.guiabolso.gbfrontapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import br.com.guiabolso.gbfrontapp.adapter.JokeCategoriesAdapter
import br.com.guiabolso.gbfrontapp.callback.JokeCategoryCallback
import br.com.guiabolso.gbfrontapp.module.APIModuleInitializer
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val call = APIModuleInitializer().chuckNorrisApiService().getJokesCategories()
        call.enqueue(JokeCategoryCallback(this))
    }

    fun configureList(categories: Array<String>) {
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = mLayoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        progressBar.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
        recyclerView.adapter = JokeCategoriesAdapter(categories, this) {
            val intent = Intent(this@MainActivity, JokeActivity::class.java)
            intent.putExtra("CATEGORY_ID", it)
            this@MainActivity.startActivity(intent)
        }
    }

}