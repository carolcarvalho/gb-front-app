package br.com.guiabolso.gbfrontapp.service

import br.com.guiabolso.gbfrontapp.model.JokeResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ChuckNorrisAPI {

    @GET("jokes/categories")
    fun  getJokesCategories() : Call<Array<String>>

    @GET("jokes/random")
    fun getJokeByCategory(@Query("category") category: String) : Call<JokeResponse>
}