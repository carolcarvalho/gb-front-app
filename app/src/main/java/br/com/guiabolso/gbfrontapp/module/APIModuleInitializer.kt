package br.com.guiabolso.gbfrontapp.module

import br.com.guiabolso.gbfrontapp.service.ChuckNorrisAPI
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class APIModuleInitializer {

    private val retrofitBuilder = Retrofit.Builder()
            .baseUrl("https://api.chucknorris.io/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    fun chuckNorrisApiService() = retrofitBuilder.create(ChuckNorrisAPI::class.java)!!
}