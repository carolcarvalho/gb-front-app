package br.com.guiabolso.gbfrontapp.callback

import android.util.Log
import br.com.guiabolso.gbfrontapp.MainActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JokeCategoryCallback(private val activity: MainActivity) : Callback<Array<String>> {

    override fun onResponse(call: Call<Array<String>>?, response: Response<Array<String>>?) {
        response?.body()?.let{ val categories = it
            activity.configureList(categories)
        }
    }

    override fun onFailure(call: Call<Array<String>>?, t: Throwable?) {
        Log.e("onFailure error", t?.message)
    }
}