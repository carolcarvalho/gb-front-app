package br.com.guiabolso.gbfrontapp.model

import br.com.guiabolso.gbfrontapp.R

class CategoryImageData {


    fun getDrawbleImage(category: String): Int {

        val drawableImagesMap = mapOf("explicit" to R.drawable.explicit,
                "dev" to R.drawable.dev,
                "movie" to R.drawable.movie,
                "food" to R.drawable.food,
                "celebrity" to R.drawable.celebrity,
                "science" to R.drawable.science,
                "sport" to R.drawable.sport,
                "political" to R.drawable.political,
                "religion" to R.drawable.religion,
                "animal" to R.drawable.animal,
                "history" to R.drawable.history,
                "music" to R.drawable.music,
                "travel" to R.drawable.travel,
                "career" to R.drawable.career,
                "money" to R.drawable.money,
                "fashion" to R.drawable.fashion)

        return drawableImagesMap.getValue(category)
    }
}