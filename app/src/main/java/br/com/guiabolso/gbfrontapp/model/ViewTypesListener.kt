package br.com.guiabolso.gbfrontapp.model

typealias ViewTypesListener<T> = (T) -> Unit