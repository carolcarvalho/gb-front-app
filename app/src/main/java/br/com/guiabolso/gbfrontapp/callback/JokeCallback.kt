package br.com.guiabolso.gbfrontapp.callback

import android.util.Log
import br.com.guiabolso.gbfrontapp.JokeActivity
import br.com.guiabolso.gbfrontapp.model.JokeResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JokeCallback(private val activity: JokeActivity) : Callback<JokeResponse> {
    override fun onResponse(call: Call<JokeResponse>?, response: Response<JokeResponse>?) {
        response?.body()?.let {
            val jokeResponse = it
            activity.configureJoke(jokeResponse.value)
        }
    }

    override fun onFailure(call: Call<JokeResponse>?, t: Throwable?) {
        Log.e("onFailure error", t?.message)
    }
}