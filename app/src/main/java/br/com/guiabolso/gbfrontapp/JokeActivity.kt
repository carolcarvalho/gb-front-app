package br.com.guiabolso.gbfrontapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import br.com.guiabolso.gbfrontapp.callback.JokeCallback
import br.com.guiabolso.gbfrontapp.model.JokeResponse
import br.com.guiabolso.gbfrontapp.module.APIModuleInitializer
import retrofit2.Call

class JokeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.jokes_description)
        val categoryId = intent.getStringExtra("CATEGORY_ID")
        categoryId?.let {
            getJokeByCategory(it)

        }

        val returnButton = findViewById<Button>(R.id.returnJokeList)
        returnButton.setOnClickListener { finish() }
        val refreshButton = findViewById<Button>(R.id.buttonRefresh)
        refreshButton.setOnClickListener { getJokeByCategory(categoryId) }
    }

    fun getJokeByCategory(jokeCategory: String): Call<JokeResponse> {
        val jokeAPIResponse = APIModuleInitializer().chuckNorrisApiService().getJokeByCategory(jokeCategory)
        jokeAPIResponse.enqueue(JokeCallback(this))
        return jokeAPIResponse
    }

    fun configureJoke(description: String) {
        val textView = findViewById<TextView>(R.id.joke_description)
        textView.text = description
    }
}